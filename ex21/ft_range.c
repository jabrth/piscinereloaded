/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_range.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jberthe <jberthe@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2024/05/17 11:47:07 by jberthe           #+#    #+#             */
/*   Updated: 2024/05/18 14:50:56 by jberthe          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
/*#include <stdio.h>*/

int	*ft_range(int min, int max)
{
	int	len;
	int	*array;
	int	i;

	len = max - min;
	if (len <= 0)
		return (NULL);
	array = malloc(len * sizeof(int));
	if (array == NULL)
		return (NULL);
	i = 0;
	while (min < max)
	{
		array[i] = min;
		i++;
		min++;
	}
	return (array);
}

/*int	main(void)
{
	int	*range;
	int	min = 3;
	int	max = 5;

	range = ft_range(min, max);
	for (int j = 0; j < max - min; j++)
	{
		printf("%d\n", range[j]);
	}
	if (range == NULL)
		free(range);
	return (0);
}
*/
